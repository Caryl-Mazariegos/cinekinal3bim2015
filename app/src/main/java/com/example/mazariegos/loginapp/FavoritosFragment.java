package com.example.mazariegos.loginapp;

import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.mazariegos.loginapp.Retrofit.Pelicula;

import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class FavoritosFragment extends android.support.v4.app.Fragment {

    ListView lvFavoritos;

    public FavoritosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favoritos, container, false);

        lvFavoritos= (ListView) view.findViewById(R.id.lvFavoritos);

        AdaptadorPeliculas adaptador = new AdaptadorPeliculas(getActivity(), MovieManager.AModelPelicula());
        lvFavoritos.setAdapter(adaptador);

        return view;
    }

    private class AdaptadorPeliculas extends ArrayAdapter<Pelicula> {
        private List<Pelicula> listaPelicula;

        public AdaptadorPeliculas(Context context, List<Pelicula> peliculas) {
            super(context, R.layout.listitem_favoritos, peliculas);
            //listaPelicula = peliculas;
        }

        public View getView(int position, View containerView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.listitem_favoritos, null);

            TextView txtTitle = (TextView) item.findViewById(R.id.title);
            txtTitle.setText(MovieManager.AModelPelicula().get(position).getTitulo());

            TextView txtGenero = (TextView)item.findViewById(R.id.Genero);
            txtGenero.setText(MovieManager.AModelPelicula().get(position).getGenero());

            return item;
        }
    }

}