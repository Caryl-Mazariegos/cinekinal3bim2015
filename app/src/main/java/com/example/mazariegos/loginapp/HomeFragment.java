package com.example.mazariegos.loginapp;




import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.mazariegos.loginapp.Retrofit.Pelicula;
import com.example.mazariegos.loginapp.Retrofit.PeliculaService;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * A simple {@link Fragment} subclass.
 */

public class HomeFragment extends Fragment {

    private ListView lvItems;
    private ListView lvLista;
    private ListView lvFavoritos;
    private ImageView img;
    private static List<Pelicula> retroMovie;
    private static ArrayList<Pelicula> listFavo = new ArrayList<>();



    public HomeFragment() {
        // Required empty public constructor
    }

    public static ArrayList<String> fav = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_home, container, false);

        lvLista = (ListView)v.findViewById(R.id.lvLista);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.5/CineKinalCM/public").build();
        PeliculaService service = restAdapter.create(PeliculaService.class);

        service.getPeliculas(new Callback<List<Pelicula>>() {
            @Override
            public void success(List<Pelicula> peliculas, Response response) {
                AdaptadorPeliculas adaptador = new AdaptadorPeliculas(getActivity(), peliculas);
                lvLista.setAdapter(adaptador);
            }


            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(getActivity(), "ERROR: "+ retrofitError.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intentPelicula = new Intent(getActivity(), TitularActivity.class);
                Pelicula peliculaSeleccionada = (Pelicula) parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", peliculaSeleccionada.getTitulo());
                extras.putString("Sinopsis", peliculaSeleccionada.getSinopsis());
                extras.putString("Genero", peliculaSeleccionada.getGenero());
                extras.putString("Rated", peliculaSeleccionada.getRated());
                intentPelicula.putExtras(extras);
                startActivity(intentPelicula);

            }

        });

        registerForContextMenu(lvLista);
        return v;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu, v, menuInfo);
        //MenuInflater inflater = getMenuInflater();
        MenuInflater inflater = getActivity().getMenuInflater();
        registerForContextMenu(lvLista);

        if(v.getId() == R.id.lvLista){
            AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
            Pelicula peliculaMenu = (Pelicula) lvLista.getAdapter().getItem(info.position);
            menu.setHeaderTitle(peliculaMenu.getTitulo());
            inflater.inflate(R.menu.menu_ctx_lista_titular, menu);
        }
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();


      /*  switch (item.getItemId()) {
            case R.id.CtxHello:
                Toast.makeText(this, "Saying hello from context menu", Toast.LENGTH_LONG).show();
                return true;*/

            switch (item.getItemId()) {
                case R.id.CtxListTitularFavorito:
                    try {

                        /*for (int x = 0; x <= MovieManager.ListaPeliculas().size(); x++) {
                            if (x == info.position) {
                                *//*MovieManager.AgregarPelicula(datosTitular[x].getTitulo());
                                ArrayAdapter<String> Favs = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1,
                                        MovieManager.AListaPelicula());
                                Toast.makeText(getActivity(), "Pelicula agregada", Toast.LENGTH_LONG).show();*//*
                                String titulo = MovieManager.ListaPeliculas().get(x).getTitulo();
                                fav.add(titulo);
                            }
                        }*/

                        Pelicula pe = new Pelicula();

                        for(int x=0; x<=retroMovie.size(); x++){
                            if (x == info.position){
                                pe.setTitulo(retroMovie.get(x).getTitulo());
                                pe.setGenero(retroMovie.get(x).getGenero());
                                MovieManager.AModelPelicula().add(pe);
                                Toast.makeText(getActivity(), "Pelicula agregada a favoritos", Toast.LENGTH_LONG).show();

                            }

                        }

                    } catch (Exception e) {
                        Toast.makeText(getActivity(), "Error al agregar pelicula", Toast.LENGTH_LONG).show();
                    }
                    return true;
                default:
                    return super.onContextItemSelected(item);
            }

        }


    private class AdaptadorPeliculas extends ArrayAdapter<Pelicula> {
        private List<Pelicula> listaPelicula;

        public AdaptadorPeliculas(Context context, List<Pelicula> peliculas){
            super(context, R.layout.listitem_titular, peliculas);
            listaPelicula = peliculas;
            retroMovie= peliculas;
        }

        public View getView(int position, View containerView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.listitem_titular, null);

            TextView txtTitle = (TextView)item.findViewById(R.id.title);
            txtTitle.setText(listaPelicula.get(position).getTitulo());

            TextView txtSinopsis = (TextView)item.findViewById(R.id.sinopsis);
            txtSinopsis.setText(listaPelicula.get(position).getSinopsis());


            String urls =  listaPelicula.get(position).getImage();

            img = (ImageView)item.findViewById(R.id.imgPelicula);

            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheOnDisc(true).cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .displayer(new FadeInBitmapDisplayer(600)).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().
                    getApplicationContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .memoryCache(new WeakMemoryCache())
                    .discCacheSize(100 * 1024 * 1024).build();

            ImageLoader.getInstance().init(config);

            ImageLoader imageLoader = ImageLoader.getInstance();
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true).build();

            imageLoader.displayImage(urls, img, options);

            return item;
        }

    }
}
