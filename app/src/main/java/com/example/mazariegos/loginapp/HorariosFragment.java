package com.example.mazariegos.loginapp;


import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mazariegos.loginapp.Retrofit.Cartelera;
import com.example.mazariegos.loginapp.Retrofit.CarteleraService;
import com.example.mazariegos.loginapp.Retrofit.Pelicula;
import com.example.mazariegos.loginapp.Retrofit.PeliculaService;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class HorariosFragment extends android.support.v4.app.Fragment {

    private ListView lvCartelera;
    private ImageView img1;

    public HorariosFragment() {
        // Required empty public constructor
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_horarios, container, false);

        lvCartelera = (ListView)v.findViewById(R.id.lvCartelera);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.5/CineKinalCM/public").build();
        CarteleraService service = restAdapter.create(CarteleraService.class);

        service.getCarteleras(new Callback<List<Cartelera>>() {
            @Override
            public void success(List<Cartelera> carteleras, Response response) {
                AdaptadorCarteleras adaptador = new AdaptadorCarteleras(getActivity(), carteleras);
                lvCartelera.setAdapter(adaptador);
            }


            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(getActivity(), "ERROR: " + retrofitError.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

        registerForContextMenu(lvCartelera);
        return v;
    }



    private class AdaptadorCarteleras extends ArrayAdapter<Cartelera> {
        private List<Cartelera> listaCartelera;

        public AdaptadorCarteleras(Context context, List<Cartelera> carteleras){
            super(context, R.layout.listitem_cartelera, carteleras);
            listaCartelera = carteleras;

        }

        public View getView(int position, View containerView, ViewGroup parent){
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.listitem_cartelera, null);

            TextView txtCine = (TextView)item.findViewById(R.id.cine);
            txtCine.setText(listaCartelera.get(position).getNombre());

            TextView txtTitulo = (TextView)item.findViewById(R.id.titulo);
            txtTitulo.setText(listaCartelera.get(position).getTitulo());

            TextView txtSala = (TextView)item.findViewById(R.id.sala);
            txtSala.setText(listaCartelera.get(position).getNumero());

            TextView txtGenero = (TextView)item.findViewById(R.id.genero2);
            txtGenero.setText(listaCartelera.get(position).getGenero());

            TextView txtRated = (TextView)item.findViewById(R.id.rated);
            txtRated.setText(listaCartelera.get(position).getRated());

            TextView txtHora = (TextView)item.findViewById(R.id.hora);
            txtHora.setText(listaCartelera.get(position).getHora());

            TextView txtFecha = (TextView)item.findViewById(R.id.fecha);
            txtFecha.setText(listaCartelera.get(position).getFecha());

            String urls =  listaCartelera.get(position).getImage();

            img1 = (ImageView)item.findViewById(R.id.imgCartelera);

            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheOnDisc(true).cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .displayer(new FadeInBitmapDisplayer(600)).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getActivity().
                    getApplicationContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .memoryCache(new WeakMemoryCache())
                    .discCacheSize(100 * 1024 * 1024).build();

            ImageLoader.getInstance().init(config);

            ImageLoader imageLoader = ImageLoader.getInstance();
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true).build();

            imageLoader.displayImage(urls, img1, options);

            return item;
        }

    }
}
