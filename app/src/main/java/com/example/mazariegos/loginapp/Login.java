package com.example.mazariegos.loginapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;


import com.example.mazariegos.loginapp.helpers.UsersSQLiteHelper;
import com.parse.LogInCallback;
import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

//import java.util.Random;


public class Login extends ActionBarActivity {


    private Button btnLogin;
    private TextView btnRegistrar;
    private EditText txtUsuarios,txtPassw;
    private Toolbar mToolbar;
    private CheckBox ChbRememberme;
    String user, pass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        Button registrar =(Button)findViewById(R.id.btnRegistrar);
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Login.this, Registre.class);
                startActivity(intent);
            }
        });

        Button login = (Button)findViewById(R.id.btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtUsuarios = (EditText) findViewById(R.id.Usuario);
                txtPassw = (EditText) findViewById(R.id.Pass);

                user = txtUsuarios.getText().toString();
                pass = txtPassw.getText().toString();

                ParseUser.logInInBackground(user, pass, new LogInCallback() {
                    @Override
                    public void done(ParseUser parseUser, ParseException e) {
                        if(parseUser !=null){

                            Bundle bundle = new Bundle();
                            bundle.putString("Nombre", parseUser.getUsername().toString());
                            bundle.putString("Correo", parseUser.getEmail().toString());

                            Intent welcome = new Intent(Login.this, Welcome.class);
                            welcome.putExtras(bundle);
                            startActivity(welcome);
                            finish();
                        }else{
                            Toast toast = Toast.makeText(getApplicationContext(), "Datos erroneos", Toast.LENGTH_SHORT);
                            toast.show();
                        }
                    }
                });
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
  }