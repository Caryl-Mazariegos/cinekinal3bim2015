package com.example.mazariegos.loginapp;


import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.support.v7.widget.Toolbar;

import com.example.mazariegos.loginapp.adapters.NavigationDrawerAdapter;
import com.example.mazariegos.loginapp.helpers.RecyclerItemClickListener;
import com.example.mazariegos.loginapp.helpers.UsersSQLiteHelper;

import java.nio.BufferUnderflowException;


/**
 * A simple {@link Fragment} subclass.
 */

public class NavigationDrawerFragment extends Fragment {

    private ActionBarDrawerToggle mDrawerToggle;
    private DrawerLayout mDrawerLayout;
    private Toolbar mToolbar;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private FragmentDrawerListener mDrawerListener;
    private View containerView;

    private int ICONS[] = {R.drawable.ic_home, R.drawable.ic_movie, R.drawable.ic_favoritos, R.drawable.ic_cartelera, R.drawable.ic_ajustes};
    private String TITLES[] = {"Inicio", "Películas","Películas favoritas", "Horarios", "Ajustes"};
    private String NAME =  null;
    private String EMAIL = null;
    private int PROFILE = R.mipmap.ic_profile;

    private UsersSQLiteHelper DBC;
    private SQLiteDatabase db;

    public NavigationDrawerFragment() {
        // Required empty public constructor
    }

    public String UserEmail(String usuario){
        String email = null;
        DBC = new UsersSQLiteHelper(getActivity().getBaseContext());
        db = DBC.getReadableDatabase();
        String Sql = "SELECT correo FROM Usuarios WHERE userName='"+usuario+"'";
        Cursor cu = db.rawQuery(Sql, null);
        if (cu.moveToFirst())
        {
            do {
                email = cu.getString(0);
            } while (cu.moveToNext());
        }
        db.close();

        return email;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);

        Bundle bundle = getActivity().getIntent().getExtras();
        NAME = bundle.getString("Nombre").toString();
        EMAIL = bundle.getString("Correo").toString();

        mRecyclerView = (RecyclerView) v.findViewById(R.id.RecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new NavigationDrawerAdapter(TITLES, ICONS, NAME, EMAIL, PROFILE);
        mRecyclerView.setAdapter(mAdapter);

        mRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                mDrawerListener.onDrawerItemSelected(view, position);
                mDrawerLayout.closeDrawer(containerView);
            }
        }));

        return v;
    }


    public void setUp(DrawerLayout drawerLaout, Toolbar toolbar, int fragmentId) {
        this.mDrawerLayout = drawerLaout;
        this.mToolbar = toolbar;
        this.containerView = getActivity().findViewById(fragmentId);

        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLaout, toolbar, R.string.drawer_open, R.string.drawer_close){

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                getActivity().invalidateOptionsMenu();
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);
                mToolbar.setAlpha(1 - slideOffset / 2);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerLayout.post(new Runnable() {
            @Override
            public void run() {
                mDrawerToggle.syncState();
            }
        });
    }

    public void setDrawerListener(FragmentDrawerListener drawerListener){
        this.mDrawerListener =  drawerListener;
    }

    public interface FragmentDrawerListener {
        public void onDrawerItemSelected(View view, int position);
    }
}