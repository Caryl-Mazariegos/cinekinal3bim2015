package com.example.mazariegos.loginapp;



import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Registre extends Activity {

    EditText txtUsuarioRegistrar, txtVerificarPass, txtPassRegistrar, txtCorreo, txtNyA;
    String user, pass, pass2, correo, nya;


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registre);



        Button enviar = (Button)findViewById(R.id.btnRegistrarUsuario);
        enviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                txtUsuarioRegistrar = (EditText) findViewById(R.id.txtUsuarioRegistrar);
                txtVerificarPass = (EditText) findViewById(R.id.txtVerificarPass);
                txtPassRegistrar = (EditText) findViewById(R.id.txtPassRegistrar);
                txtCorreo = (EditText) findViewById(R.id.txtCorreo);
                txtNyA = (EditText) findViewById(R.id.txtNyA);

                user = txtUsuarioRegistrar.getText().toString();
                pass = txtPassRegistrar.getText().toString();
                pass2 = txtVerificarPass.getText().toString();
                correo = txtCorreo.getText().toString();
                nya = txtNyA.getText().toString();

                ParseUser user1 = new ParseUser();

                user1.setUsername(user);
                user1.setPassword(pass);
                user1.put("pass2", pass2);
                user1.setEmail(correo);
                user1.put("NyA", nya);


                user1.signUpInBackground(new SignUpCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e != null) {
                            Toast.makeText(Registre.this, "Fallo inicio: " + e.getMessage(), Toast.LENGTH_LONG).show();
                        } else {
                            Intent intent = new Intent(Registre.this, Login.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                });
            }

        });

    }
}