package com.example.mazariegos.loginapp.Retrofit;

/**
 * Created by toto on 29/07/15.
 */
public class Cartelera {

    private int id;
    private String nombre;
    private String titulo;
    private String image;
    private String rated;
    private String genero;
    private String fecha;
    private String hora;
    private String numero;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }


    public Cartelera () {
    }

    public Cartelera(String nombre, String titulo, String image, String rated, String genero, String fecha, String hora, String numero) {

        this.nombre = nombre;
        this.titulo = titulo;
        this.image = image;
        this.rated = rated;
        this.genero = genero;
        this.fecha = fecha;
        this.hora = hora;
        this.numero = numero;
    }

}
