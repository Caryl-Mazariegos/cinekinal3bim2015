package com.example.mazariegos.loginapp.Retrofit;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;


public interface CarteleraService {

    @GET("/carteleras")
    void getCarteleras(Callback<List<Cartelera>> callback);
}