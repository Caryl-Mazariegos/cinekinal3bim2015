package com.example.mazariegos.loginapp.Retrofit;


public class Estreno {

    private int id;
    private String titulo;
    private String sinopsis;
    private String trailer_url;
    private String rated;
    private String image;
    private String genero;
    private String fecha;
    private String tipo;

    public String getTipo() {
        return tipo;
    }

    public void getTipo(String fecha) {
        this.tipo = tipo;
    }


    public String getFecha() {
        return fecha;
    }

    public void getFecha(String fecha) {
        this.fecha = fecha;
    }


    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }


    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Estreno () {
    }

    public Estreno(int id,String image, String titulo, String sinopsis, String trailer_url, String rated, String genero, String fecha, String tipo) {
        this.id = id;
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.trailer_url = trailer_url;
        this.rated = rated;
        this.genero = genero;
        this.fecha = fecha;
        this.tipo = tipo;
        this.image = image;
    }

}