package com.example.mazariegos.loginapp.Retrofit;

        import java.util.List;
        import retrofit.Callback;
        import retrofit.http.GET;

public interface EstrenoService {

    @GET("/estrenos")
    void getEstrenos(Callback<List<Estreno>> callback);
}