package com.example.mazariegos.loginapp.Retrofit;


import android.media.Image;

public class Pelicula {

    private int id;
    private String titulo;
    private String sinopsis;
    private String trailer_url;
    private String image;
    private String rated;
    private String genero;

    public String getSinopsis() {
        return sinopsis;
    }

    public void setSinopsis(String sinopsis) {
        this.sinopsis = sinopsis;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getRated() {
        return rated;
    }

    public void setRated(String rated) {
        this.rated = rated;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Pelicula () {
    }

    public Pelicula(int id, String titulo, String sinopsis, String trailer_url, String image, String rated, String genero) {
        this.id = id;
        this.titulo = titulo;
        this.sinopsis = sinopsis;
        this.trailer_url = trailer_url;
        this.image = image;
        this.rated = rated;
        this.genero = genero;
    }

}
