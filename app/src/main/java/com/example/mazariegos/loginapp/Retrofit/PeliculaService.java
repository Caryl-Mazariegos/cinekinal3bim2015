package com.example.mazariegos.loginapp.Retrofit;

import java.util.List;
import retrofit.Callback;
import retrofit.http.GET;

public interface PeliculaService {

    @GET("/peliculas")
    void getPeliculas(Callback<List<Pelicula>> callback);
}
