package com.example.mazariegos.loginapp;


import android.media.session.MediaController;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.VideoView;

import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;

import org.w3c.dom.Text;

import java.util.Timer;


//

public class TitularActivity extends ActionBarActivity {

    private TextView tvTitulo;
    private TextView tvSinopsis;
    private TextView tvGenero;
    private TextView tvRated;
    private ImageView tvImagen;




    private android.support.v7.widget.Toolbar mToolbar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_titular);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);



        tvTitulo = (TextView)findViewById(R.id.Titulo);
        tvSinopsis = (TextView)findViewById(R.id.Sinopsis);
        tvGenero = (TextView)findViewById(R.id.Genero);
        tvRated = (TextView)findViewById(R.id.Rated);
        tvImagen = (ImageView)findViewById(R.id.imgPelicula);


        Bundle extras = getIntent().getExtras();

        tvTitulo.setText(extras.getString("Titulo"));
        tvSinopsis.setText(extras.getString("Sinopsis"));
        tvGenero.setText(extras.getString("Genero"));
        tvRated.setText(extras.getString("Rated"));


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_titular, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        if (id == R.id.home)
            finish();

        return super.onOptionsItemSelected(item);
    }
}

