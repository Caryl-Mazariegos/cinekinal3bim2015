package com.example.mazariegos.loginapp;

import android.content.Context;

import com.example.mazariegos.loginapp.helpers.UsersSQLiteHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mazariegos on 20/05/2015.
 */
public class UserManager {
    List<Usuario> Usuarios = new ArrayList<Usuario>();
    private static UserManager instancia;
    UsersSQLiteHelper usuarios;

    public void agregarUsuario(String nombreUsuario, String pass, String NombreyApellido, String Correo) {
        Usuarios.add(new Usuario(nombreUsuario, pass, NombreyApellido, Correo));
    }

    public List<Usuario> obtenerUsuarios() {
        return this.Usuarios;
    }

    public static UserManager getInstancia() {
        if (instancia == null)
            instancia = new UserManager();
        return instancia;
    }
}
