package com.example.mazariegos.loginapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.mazariegos.loginapp.helpers.UsersSQLiteHelper;

/**
 * Created by Mazariegos on 20/05/2015.
 */
public class Usuario {

    private String nombreUsuario;
    private String pass;
    private String NombreyApellido;
    private String Correo;

    private UsersSQLiteHelper DBC;
    private SQLiteDatabase db;

    public Usuario(Context context) {
        DBC = new UsersSQLiteHelper(context);
        db = DBC.getWritableDatabase();
    }

    private ContentValues generarContentValues(String usuario, String password, String nombre, String correo) {
        ContentValues valores = new ContentValues();
        valores.put("usuario", usuario);
        valores.put("password", password);
        valores.put("nombre", nombre);
        valores.put("correo", correo);
        return valores;
    }

    public void insertar2(String nombreUsuario, String pass, String nombreyApellido, String correo) {
        db.execSQL("insert into Usuarios values (null,'"+nombreUsuario+"','"+pass+"','"+nombreyApellido+"', '"+correo+"');");
    }

    public void modificarUsuario(String usuario, String password, String nombre, String correo) {
        /*bd.update(TABLA, ContentValues, Clausula Where, Argumentos Where)*/
        //db.update("Usuarios", generarContentValues(usuario, password, nombre, correo), usuario+"=?", new String[]{usuario});
        db.execSQL("UPDATE Usuarios SET NombreyApellido='"+nombre+"', pass='"+password+"', Correo='"+correo+"'" +
                " WHERE nombreUsuario= '"+usuario+"'" );
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }
    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
    public String getNombreyApellido(){
        return NombreyApellido;
    }

    public void setNombreyApellido(String NombreyApellido) {
        this.NombreyApellido = NombreyApellido;
    }

    public String getCorreo(){
        return Correo;
    }

    public void setCorreo(String Correo) {
        this.Correo = Correo;
    }

    public Usuario(String nombreUsuario ,String pass, String NombreyApellido, String Correo){
        this.nombreUsuario = nombreUsuario;
        this.NombreyApellido = NombreyApellido;
        this.Correo = Correo;
        this.pass = pass;
    }
}
