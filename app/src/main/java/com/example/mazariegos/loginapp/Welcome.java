package com.example.mazariegos.loginapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.session.MediaController;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;


import com.example.mazariegos.loginapp.Retrofit.Estreno;
import com.example.mazariegos.loginapp.Retrofit.EstrenoService;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;


import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class Welcome extends ActionBarActivity implements NavigationDrawerFragment.FragmentDrawerListener {

    private Toolbar mToolbar;
    private TextView mHello;
    String us;
    private ListView lvELista;
    private ImageView img;
    private VideoView trailerPelicula;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);


        NavigationDrawerFragment drawerFragment = (NavigationDrawerFragment) getSupportFragmentManager().findFragmentById(R.id.navigation_drawer_fragmnet);

        drawerFragment.setUp((DrawerLayout) findViewById(R.id.drawer_layout), mToolbar, R.id.navigation_drawer_fragmnet);
        drawerFragment.setDrawerListener(this);

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(Welcome.this);

        boolean notificationsEnable = preferences.getBoolean("is_notification_enable", false);
        Toast.makeText(this, "Notificaciones: " + notificationsEnable, Toast.LENGTH_LONG).show();

        lvELista = (ListView)findViewById(R.id.lvELista);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://192.168.1.5/CineKinalCM/public").build();
        EstrenoService service = restAdapter.create(EstrenoService.class);

        service.getEstrenos(new Callback<List<Estreno>>() {
            @Override
            public void success(List<Estreno> estrenos, Response response) {
                AdaptadorEstrenos adaptador = new AdaptadorEstrenos(Welcome.this, estrenos);
                lvELista.setAdapter(adaptador);
            }


            @Override
            public void failure(RetrofitError retrofitError) {
                Toast.makeText(Welcome.this, "ERROR: " + retrofitError.getMessage(), Toast.LENGTH_SHORT).show();

            }

        });

        lvELista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent intentEstreno = new Intent(Welcome.this, TitularActivity.class);
                Estreno peliculaSeleccionada = (Estreno) parent.getItemAtPosition(position);
                Bundle extras = new Bundle();
                extras.putString("Titulo", peliculaSeleccionada.getTitulo());
                extras.putString("Sinopsis", peliculaSeleccionada.getSinopsis());
                extras.putString("Genero", peliculaSeleccionada.getGenero());
                extras.putString("Rated", peliculaSeleccionada.getRated());
                extras.putString("Fecha", peliculaSeleccionada.getFecha());
                extras.putString("Tipo", peliculaSeleccionada.getTipo());
                extras.putString("Imagen", peliculaSeleccionada.getImage());
                intentEstreno.putExtras(extras);
                startActivity(intentEstreno);

            }


        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent settingsIntent = new Intent(Welcome.this, Login.class);
            startActivity(settingsIntent);
            finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        Toast.makeText(this, "Click en la posicion: " + position, Toast.LENGTH_LONG).show();
        Fragment fragment = null;
        String title = getString(R.string.app_name);

        switch (position) {
            case 1:
                Intent intent = new Intent(Welcome.this, Welcome.class);
                startActivity(intent);
                finish();
                title = "Movies";
                break;
            case 2:
                fragment = new HomeFragment();
                title = "Películas";
                break;
            case 3:
                fragment = new FavoritosFragment();
                title = "Películas favoritas";
                break;
            case 4:
                fragment = new HorariosFragment();
                title = "Horarios";
                break;
            case 5:
                fragment = new FavoritosFragment();
                title = "Películas favoritas";
                break;
            default:
                break;
        }

        if (fragment != null) {
            ((RelativeLayout) findViewById(R.id.ContainerLayout)).removeAllViewsInLayout();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.ContainerLayout, fragment);
            fragmentTransaction.commit();
            getSupportActionBar().setTitle(title);
        }
    }

    private class AdaptadorEstrenos extends ArrayAdapter<Estreno> {
        private List<Estreno> listaEstreno;

        public AdaptadorEstrenos(Context context, List<Estreno> estrenos) {
            super(context, R.layout.listitem_estreno, estrenos);
            listaEstreno = estrenos;

        }

        public View getView(int position, View containerView, ViewGroup parent) {
            LayoutInflater inflater = LayoutInflater.from(getContext());

            View item = inflater.inflate(R.layout.listitem_estreno, null);

            TextView txtTitle = (TextView) item.findViewById(R.id.title);
            txtTitle.setText(listaEstreno.get(position).getTitulo());

            TextView txtFecha = (TextView) item.findViewById(R.id.fecha);
            txtFecha.setText(listaEstreno.get(position).getFecha());

            TextView txtTipo = (TextView) item.findViewById(R.id.tipo);
            txtTipo.setText(listaEstreno.get(position).getTipo());

            String urls =  listaEstreno.get(position).getImage();

            img = (ImageView)item.findViewById(R.id.imgPelicula);

            DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                    .cacheOnDisc(true).cacheInMemory(true)
                    .imageScaleType(ImageScaleType.EXACTLY)
                    .displayer(new FadeInBitmapDisplayer(600)).build();

            ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                    getApplicationContext())
                    .defaultDisplayImageOptions(defaultOptions)
                    .memoryCache(new WeakMemoryCache())
                    .discCacheSize(100 * 1024 * 1024).build();

            ImageLoader.getInstance().init(config);

            ImageLoader imageLoader = ImageLoader.getInstance();
            DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                    .cacheOnDisc(true).resetViewBeforeLoading(true).build();

            imageLoader.displayImage(urls, img, options);

            return item;

        }

    }

}

