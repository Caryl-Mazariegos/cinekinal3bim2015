package com.example.mazariegos.loginapp.helpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Caryl Mazariegos on 07/06/2015.
 */

public class UsersSQLiteHelper extends SQLiteOpenHelper {

    private String sqlFavorites = "CREATE TABLE Favorites (idFav INTEGER PRIMARY KEY, idPeliculas INTEGER,idUsuarios INTEGER ,FOREIGN KEY(idPeliculas) REFERENCES Peliculas(idPeliculas),FOREIGN KEY(idUsuarios) REFERENCES Usuarios(idUsuarios));";
    private static final String nameDataBase = "DBcine";
    private static final int version = 1;



    public UsersSQLiteHelper(Context context) {
        super(context, nameDataBase, null, version);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {


        db.execSQL(sqlFavorites);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS Favorites");

        db.execSQL(sqlFavorites);

    }
}
