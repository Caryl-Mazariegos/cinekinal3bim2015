package com.example.mazariegos.loginapp.models;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.example.mazariegos.loginapp.helpers.UsersSQLiteHelper;

/**
 * Created by Mazariegos on 20/05/2015.
 */
public class Titular {


    private String titulo;
    private String subtitulo;
    private String descripcion;

    public String getTitulo() {
        return titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public Titular(String titulo, String subtitulo, String descripcion) {
        this.titulo = titulo;
        this.subtitulo = subtitulo;
        this.descripcion = descripcion;
    }
}

