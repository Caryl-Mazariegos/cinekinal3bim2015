CREATE DATABASE cine;

USE cine;

CREATE TABLE Pelicula(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    titulo VARCHAR(100) NOT NULL,
    sinopsis VARCHAR(500) NOT NULL,
    trailer_url VARCHAR(200) NOT NULL,
    image VARCHAR(500) NOT NULL,
    rated VARCHAR(20) NOT NULL,
    genero VARCHAR(50) NOT NULL
);


CREATE TABLE FormatoPelicula(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(200) NOT NULL
);

CREATE TABLE Cine(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    nombre VARCHAR(200) NOT NULL,
    direccion VARCHAR(500) NOT NULL,
    telefono VARCHAR(10) NOT NULL,
    latitud DOUBLE(20, 18) NOT NULL,
    longitud DOUBLE(20, 18) NOT NULL,
    hora_apertura TIME NOT NULL,
    hora_cierre TIME NOT NULL
);

CREATE TABLE Estreno(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	cine_id int(11) NOT NULL,
    titulo VARCHAR(100) NOT NULL,
	tipo varchar(50) NOT NULL,
    sinopsis VARCHAR(500) NOT NULL,
    trailer_url VARCHAR(200) NOT NULL,
	precio VARCHAR(200) NOT NULL,
    image VARCHAR(500) NOT NULL,
    rated VARCHAR(20) NOT NULL,
    genero VARCHAR(50) NOT NULL,
	fecha DATE NOT NULL,
	FOREIGN KEY (cine_id) REFERENCES Cine(id)
	
);

CREATE TABLE TipoSala(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nombre VARCHAR(50) NOT NULL,
    descripcion VARCHAR(200) NOT NULL
);

CREATE TABLE Sala(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    cine_id INT NOT NULL,
    numero INT NOT NULL,
    tiposala_id INT NOT NULL,
    FOREIGN KEY (cine_id) REFERENCES Cine(id),
    FOREIGN KEY (tiposala_id) REFERENCES TipoSala(id)
);

CREATE TABLE Cartelera(
	id INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    sala_id INT NOT NULL,
    pelicula_id INT NOT NULL,
    formatopelicula_id INT NOT NULL,
    formato_lenguaje VARCHAR(10) NOT NULL,
    fecha DATE NOT NULL,
    hora TIME NOT NULL,
    FOREIGN KEY (sala_id) REFERENCES Sala(id),
    FOREIGN KEY (pelicula_id) REFERENCES Pelicula(id),
    FOREIGN KEY (formatopelicula_id) REFERENCES FormatoPelicula(id)
);

INSERT INTO `cine`.`Cine` (`id`, `nombre`, `direccion`, `telefono`, `latitud`, `longitud`, `hora_apertura`, `hora_cierre`) VALUES ('1', 'Lux', 'zona1', '45645616', '0.111100000000000000', '0.333300000000000000', '06:00:00', '20:00:00');
INSERT INTO `cine`.`Cine` (`id`, `nombre`, `direccion`, `telefono`, `latitud`, `longitud`, `hora_apertura`, `hora_cierre`) VALUES ('2', 'Miraflores', 'Zona7', '245454', '0.2222222222222', '0.4444444444444444', '06:00:00', '20:00:00');
INSERT INTO `cine`.`Cine` (`id`, `nombre`, `direccion`, `telefono`, `latitud`, `longitud`, `hora_apertura`, `hora_cierre`) VALUES ('3', 'Okland', 'Zona12', '46578641', '0.33333333333333', '0.2222222222222222', '06:00:00', '20:00:00');
INSERT INTO `cine`.`Cine` (`id`, `nombre`, `direccion`, `telefono`, `latitud`, `longitud`, `hora_apertura`, `hora_cierre`) VALUES ('4', 'Capitols', 'Zona1', '4564616', '0.444444444444', '0.111111111111111111', '06:00:00', '20:00:00');

INSERT INTO `cine`.`Pelicula` (`id`, `titulo`, `sinopsis`, `trailer_url`, `image`, `rated`, `genero`) VALUES ('1', 'Avengers2', 'En Sokovia, un país ficticio de Europa Oriental, los Vengadores - Iron Man, Capitán América, Thor, Hulk, Black Widow y Hawkeye - atacan un puesto avanzado de HYDRA dirigido por el Barón Wolfgang von Strucker, que ha estado experimentando con humanos usando el cetro previamente usado por Loki.', 'https://www.youtube.com/watch?v=u1OKBqHICMQ', 'http://comoentv.biz/wp-content/uploads/2014/06/advengers.jpg', 'A', 'Acción');
INSERT INTO `cine`.`Pelicula` (`id`, `titulo`, `sinopsis`, `trailer_url`, `image`, `rated`, `genero`) VALUES ('2', 'Ant-Man', 'El ladrón Scott Lang debe ayudar a su mentor, el Dr. Hank Pym, a salvaguardar el misterio de la tecnología de Ant-Man –que permite a su usuario disminuir el tamaño, pero aumentar en fuerza– de varias amenazas', 'https://www.youtube.com/watch?v=pWdKf3MneyI', 'http://cdn3-www.superherohype.com/assets/uploads/gallery/ant-man_7925/antman-poster.jpg', 'A', 'Acción');
INSERT INTO `cine`.`Pelicula` (`id`, `titulo`, `sinopsis`, `trailer_url`, `image`, `rated`, `genero`) VALUES ('3', 'Suicide Squad', 'Durante la Segunda Guerra Mundial, un grupo de soldados indisciplinados e irrespetuosos se reunió para combatir y cumplir en misiones encubiertas para el gobierno de los Estados Unidos. El equipo original pasó gran parte de su período de servicio en la Isla de los Dinosaurios. Estos soldados (considerados desechables) fueron apodados el Escuadrón Suicida.', 'https://www.youtube.com/watch?v=PLLQK9la6Go', 'https://pmcvariety.files.wordpress.com/2015/05/suicide-squad-1.jpg?w=670&h=377&crop=1', 'A', 'Acción');
INSERT INTO `cine`.`Pelicula` (`id`, `titulo`, `sinopsis`, `trailer_url`, `image`, `rated`, `genero`) VALUES ('4', 'Capitan America', 'Capitán América (Captain America en inglés) es un personaje ficticio propiedad de Marvel Comics. La serie fue creada en 1941 por los historietistas Joe Simon y Jack Kirby, meses antes de que Estados Unidos entrase en la Segunda Guerra Mundial.', 'https://www.youtube.com/watch?v=NvZ1BXQornk', 'http://blogs.rpp.com.pe/technovida/files/2011/07/trailer-espanol-capitan-america-vengador_1_639169.jpg', 'A', 'Acción');
INSERT INTO `cine`.`Pelicula` (`id`, `titulo`, `sinopsis`, `trailer_url`, `image`, `rated`, `genero`) VALUES ('5', '4 Fantasticos', 'Los 4 Fantásticos es un equipo de superhéroes ficticio que aparece en cómics estadounidenses publicados por Marvel Comics. El grupo debutó en The Fantastic Four #1 (Noviembre de 1961), el cual ayudó a marcar el comienzo de un nuevo nivel de realismo en el medio.', 'https://www.youtube.com/watch?v=L3gc6hOqNrY', 'http://img.photobucket.com/albums/v124/dragancete/4Fantasticos.jpg', 'A', 'Acción');
INSERT INTO `cine`.`Pelicula` (`id`, `titulo`, `sinopsis`, `trailer_url`, `image`, `rated`, `genero`) VALUES ('6', 'X men', 'X-Men, también conocidos como Patrulla X en España o Los hombres X en algunos países de Latinoamerica, son un grupo de superhéroes del Universo Marvel creado por Stan Lee y Jack Kirby y tuvo su primera aparición en septiembre de 1963.', 'https://www.youtube.com/watch?v=xFCxOFDwTKs', 'https://www.vidangel.com/wp-content/uploads/2014/08/x-men-the-last-stand.jpg', 'A', 'Acción');

INSERT INTO `cine`.`Estreno` (`id`, `cine_id`, `titulo`, `tipo`, `sinopsis`, `trailer_url`, `precio`, `image`, `rated`, `genero`, `fecha`) VALUES ('1', '1', 'Batman', 'Estreno', 'Batman v Superman: Dawn of Justice es una película del género de superhéroes que será estrenada el 25 de marzo de 2016. Es una secuela indirecta de la película Man of Steel pero a la vez funciona como película aparte. ', 'https://www.youtube.com/watch?v=xe1LrMqURuw', '50', 'http://vignette1.wikia.nocookie.net/batman/images/b/b2/Movie.jpg/revision/latest?cb=20150108135154&path-prefix=es', 'C', 'Accion', '2015-02-02');
INSERT INTO `cine`.`Estreno` (`id`, `cine_id`, `titulo`, `tipo`, `sinopsis`, `trailer_url`, `precio`, `image`, `rated`, `genero`, `fecha`) VALUES ('2', '2', 'El conjuro', 'Preventa', 'The Conjuring es una película de terror de 2013 dirigida por James Wan y protagonizada por Vera Farmiga y Patrick Wilson en el papel de los parapsicólogos Lorraine y Ed Warren.', 'https://www.youtube.com/watch?v=chAT_cFcQk0', '150', 'http://www.teletica.com/Multimedios/imgs/37872_620.jpg', 'C', 'Terror', '2015-02-20');
INSERT INTO `cine`.`Estreno` (`id`, `cine_id`, `titulo`, `tipo`, `sinopsis`, `trailer_url`, `precio`, `image`, `rated`, `genero`, `fecha`) VALUES ('3', '3', 'Civil War', 'Estreno', 'Captain America: Civil War es una futura película de superhéroes, a estrenarse el 6 de mayo de 2016. Es una secuela de Captain America: The Winter Soldier y la última película del Capitán América confirmada por ahora.', 'https://www.youtube.com/watch?v=n0_x5bqospk', '50', 'http://img.informador.com.mx/biblioteca/imagen/370x277/1169/1168875.jpg', 'A', 'Accion', '2015-02-03');
INSERT INTO `cine`.`Estreno` (`id`, `cine_id`, `titulo`, `tipo`, `sinopsis`, `trailer_url`, `precio`, `image`, `rated`, `genero`, `fecha`) VALUES ('4', '4', 'Intensamente', 'Preventa', 'Inside Out (titulada Del revés en España e Intensa-Mente en Hispanoamérica) es una película estadounidense de comedia animada en 3D, producida por Pixar Animation Studios y distribuida por Walt Disney Pictures. La película está basada en una idea original de Pete Docter, quien dirige la película junto al co-director Ronnie del Carmen, y el productor Jonas Rivera.', 'https://www.youtube.com/watch?v=EB1oBuxD964', '150', 'http://elestimulo.com/wp-content/uploads/2015/06/pp151-1100x618.jpg', 'A', 'Comedia', '2015-02-15');

INSERT INTO `cine`.`TipoSala` (`id`, `nombre`, `descripcion`) VALUES ('1', '2d', 'Tecnología 2d');
INSERT INTO `cine`.`TipoSala` (`id`, `nombre`, `descripcion`) VALUES ('2', '3d', 'Tecnología 3d');

INSERT INTO `cine`.`FormatoPelicula` (`id`, `nombre`, `descripcion`) VALUES ('1', 'Doblada', 'Lenguaje español');
INSERT INTO `cine`.`FormatoPelicula` (`id`, `nombre`, `descripcion`) VALUES ('2', 'Subtitulada', 'Subtitulada al español');

INSERT INTO `cine`.`Cartelera` (`id`, `sala_id`, `pelicula_id`, `formatopelicula_id`, `formato_lenguaje`, `fecha`, `hora`) VALUES ('1', '1', '1', '1', 'español', '2015-02-02', '16:00:00');
INSERT INTO `cine`.`Cartelera` (`id`, `sala_id`, `pelicula_id`, `formatopelicula_id`, `formato_lenguaje`, `fecha`, `hora`) VALUES ('2', '2', '2', '1', 'español', '2015-02-02', '12:00:00');
INSERT INTO `cine`.`Cartelera` (`id`, `sala_id`, `pelicula_id`, `formatopelicula_id`, `formato_lenguaje`, `fecha`, `hora`) VALUES ('3', '3', '3', '2', 'español', '2015-02-02', '14:00:00');
INSERT INTO `cine`.`Cartelera` (`id`, `sala_id`, `pelicula_id`, `formatopelicula_id`, `formato_lenguaje`, `fecha`, `hora`) VALUES ('4', '4', '4', '1', 'español', '2015-02-02', '17:00:00');
INSERT INTO `cine`.`Cartelera` (`id`, `sala_id`, `pelicula_id`, `formatopelicula_id`, `formato_lenguaje`, `fecha`, `hora`) VALUES ('5', '5', '5', '1', 'español', '2015-02-02', '18:00:00');


INSERT INTO `cine`.`Sala` (`id`, `cine_id`, `numero`, `tiposala_id`) VALUES ('1', '1', '1', '1');
INSERT INTO `cine`.`Sala` (`id`, `cine_id`, `numero`, `tiposala_id`) VALUES ('2', '1', '2', '2');
INSERT INTO `cine`.`Sala` (`id`, `cine_id`, `numero`, `tiposala_id`) VALUES ('3', '2', '1', '1');
INSERT INTO `cine`.`Sala` (`id`, `cine_id`, `numero`, `tiposala_id`) VALUES ('4', '2', '2', '2');
INSERT INTO `cine`.`Sala` (`id`, `cine_id`, `numero`, `tiposala_id`) VALUES ('5', '3', '1', '2');
